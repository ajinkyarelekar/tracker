//
//  ViewModel.swift
//  NEC
//
//  Created by venajr on 21/4/21.
//

import Foundation
import AVKit
class ViewModel {
    let captureSession = AVCaptureSession()
    let captureSessionQueue = DispatchQueue(label: Constants.captureQueueLabel)
    var videoOutput = AVCaptureVideoDataOutput()
    var prevImage: UIImage?
    
    func checkPermission(isGranted: @escaping (Bool)->Void) {
        if AVCaptureDevice.authorizationStatus(for: .video) != .authorized {
            captureSessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video) { [unowned self] (granted) in
                self.captureSessionQueue.resume()
                isGranted(granted)
            }
        } else {
            isGranted(true)
        }
    }
    
    func configureSession(withInputView viewIn: UIView, captureDelegate: AVCaptureVideoDataOutputSampleBufferDelegate) {
        captureSession.sessionPreset = Constants.captureQuality
        let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        let deviceInput = try! AVCaptureDeviceInput(device: captureDevice!)
        if captureSession.canAddInput(deviceInput) {
            captureSession.addInput(deviceInput)
        }
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspect
        DispatchQueue.main.async {
            previewLayer.frame = viewIn.bounds
            viewIn.layer.addSublayer(previewLayer)
        }
        videoOutput = AVCaptureVideoDataOutput()
        videoOutput.connection(with: .video)?.videoOrientation = .portrait
        videoOutput.setSampleBufferDelegate(captureDelegate, queue: DispatchQueue(label: Constants.captureQueueLabel))
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        }
    }
    
    func startSession(withInputView viewIn: UIView, captureDelegate: AVCaptureVideoDataOutputSampleBufferDelegate) {
        captureSessionQueue.async { [unowned self] in
            configureSession(withInputView: viewIn, captureDelegate: captureDelegate)
            captureSession.startRunning()
        }
    }
    
    func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        let context = CIContext()
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage).rotate(radians: .pi/2)
    }
    
    func compare(newImage: UIImage, completion: @escaping ((UIImage)->Void)) {
        OpenCVWrapper.compareNewImage(newImage, withOldImage: prevImage ?? newImage) {[unowned self] (difference) in
            prevImage = newImage
            completion(difference)
        }
    }
}




