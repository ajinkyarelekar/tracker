//
//  ViewController.swift
//  NEC
//
//  Created by venajr on 20/4/21.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    @IBOutlet weak var viewIn: UIView!
    @IBOutlet weak var imgViewOut: UIImageView!
    let viewModel = ViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel.checkPermission {[unowned self] (isGranted) in
            if isGranted {
                viewModel.startSession(withInputView: viewIn, captureDelegate: self)
            } else {
                Helpers.showAlertWithAction(withTitle: "Permission Denied!", withMessage: "Please give permission to use camera.") {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }
            }
        }
    }
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if let img = viewModel.imageFromSampleBuffer(sampleBuffer: sampleBuffer) {
            viewModel.compare(newImage: img) { [unowned self] (imgDiff) in
                DispatchQueue.main.async {
                    imgViewOut.image = imgDiff
                }
            }
        }
    }
}
