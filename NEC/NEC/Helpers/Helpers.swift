//
//  Helpers.swift
//  NEC
//
//  Created by venajr on 21/4/21.
//

import Foundation
class Helpers {
    
    static func showAlertWithAction(withTitle title: String, withMessage message: String, completion: @escaping ()->Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (act) in
            alert.dismiss(animated: true, completion: nil)
            completion()
        }))
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
}
