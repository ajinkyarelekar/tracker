//
//  AppDelegate.swift
//  NEC
//
//  Created by venajr on 20/4/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let rootController = mainStoryBoard.instantiateInitialViewController()
        self.window?.rootViewController = rootController
        self.window?.makeKeyAndVisible()
        return true
    }

}

