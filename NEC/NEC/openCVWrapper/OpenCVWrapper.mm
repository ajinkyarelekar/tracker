//
//  OpenCVWrapper.m
//  NEC
//
//  Created by venajr on 20/4/21.
//
#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#import <opencv2/imgcodecs/ios.h>
@implementation OpenCVWrapper
+ (void)compareNewImage:(UIImage *)newImage withOldImage:(UIImage *)oldImage completion:(void(^)(UIImage*))completionBlock {
    cv::Mat newMat;
    cv::Mat oldMat;
    cv::Mat finalMat;
    UIImageToMat(newImage, newMat);
    UIImageToMat(oldImage, oldMat);
    cv::subtract(newMat, oldMat, finalMat);
    cv::Mat filterMat;
    cv::cvtColor(finalMat, filterMat, cv::COLOR_BGR2GRAY);
    cv::Mat mask;
    cv::threshold(filterMat, mask, 0, 255, cv::THRESH_OTSU);
    finalMat.setTo(cv::Scalar(0, 0, 255), mask != 255);
    completionBlock(MatToUIImage(filterMat));
}
@end
