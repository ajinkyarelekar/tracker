//
//  OpenCVWrapper.h
//  NEC
//
//  Created by venajr on 20/4/21.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenCVWrapper : NSObject
+ (void)compareNewImage:(UIImage *)newImage withOldImage:(UIImage *)oldImage completion:(void(^)(UIImage*))completionBlock;
@end

NS_ASSUME_NONNULL_END
