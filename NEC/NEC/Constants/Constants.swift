//
//  Constants.swift
//  NEC
//
//  Created by venajr on 21/4/21.
//

import Foundation
import AVKit

struct Constants {
    static let captureQueueLabel = "captureQueue"
    static let captureQuality = AVCaptureSession.Preset.medium

}
